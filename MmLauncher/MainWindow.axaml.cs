using System.Threading;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Threading;

namespace MmLauncher;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        LoginState.IsVisible = true;
        LoginState.IsEnabled = true;
        LoadingState.IsVisible = false;
        LoadingState.IsEnabled = false;
        MainState.IsVisible = false;
        MainState.IsEnabled = false;
        SettingsState.IsVisible = false;
        SettingsState.IsEnabled = false;
        SettingsButton.IsEnabled = false;
        SettingsButton.IsVisible = false;
    }

    public void OnClickLoginButton(object sender, RoutedEventArgs args)
    {
        LoginState.IsVisible = false;
        LoginState.IsEnabled = false;
        LoadingState.IsVisible = true;
        LoadingState.IsEnabled = true;

        Thread thread = new Thread(() =>
        {
            Dispatcher.UIThread.Post(() => SetLoadingMessage("A"));
            Thread.Sleep(1000);
            Dispatcher.UIThread.Post(() => SetLoadingMessage("B"));
        });
        thread.Start();
    }

    private void SetLoadingMessage(string message)
    {
        LoadingMessage.Text = message;
    }
    
    public void OnClickPlayButton(object sender, RoutedEventArgs args)
    {
        SetLoadingMessage("Loading");
    }

    public void OnClickSettingsButton(object sender, RoutedEventArgs args)
    {
        MainState.IsVisible = false;
        MainState.IsEnabled = false;
        SettingsState.IsVisible = true;
        SettingsState.IsEnabled = true;
    }

    public void OnClickSettingsBackButton(object sender, RoutedEventArgs args)
    {
        MainState.IsVisible = true;
        MainState.IsEnabled = true;
        SettingsState.IsVisible = false;
        SettingsState.IsEnabled = false;
    }
    
    public void OnClickCloseButton(object sender, RoutedEventArgs args)
    {
        Close();
    }
}